package hibernate;

import hibernate.DAO.MsisdnDao;
import hibernate.Entity.Msisdn;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "MsisdnView")
public class MsisdnView implements Serializable {
    private List<Msisdn> msisdnLst;

    private static final MsisdnDao msisdnDao = new MsisdnDao();

    public MsisdnView() {
        msisdnLst = msisdnDao.getMSISDN();
    }

    public List<Msisdn> getMSISDN() {
        return msisdnLst;
    }
}
