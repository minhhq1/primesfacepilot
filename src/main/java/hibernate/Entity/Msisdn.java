package hibernate.Entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Entity
@Table(name = "msisdn")
public class Msisdn {
    @Id
    @GeneratedValue(generator = "sequence")
    @GenericGenerator(name = "sequence", strategy = "sequence",
            parameters = {
                    @Parameter(name = "sequence", value = "AUTO_ID")
            })
    @Column(name = "ID")
    private int id;

    @Column(name = "MSISDN")
    private String msisdn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Msisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Msisdn() {
    }
}
