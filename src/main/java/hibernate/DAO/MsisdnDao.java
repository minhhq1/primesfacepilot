package hibernate.DAO;

import java.util.ArrayList;
import java.util.List;

import hibernate.Entity.Msisdn;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class MsisdnDao {
    private SessionFactory factory;

    public void initCore() {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public SessionFactory getFac() {
        try {
            factory = new Configuration().configure().buildSessionFactory();
            return factory;
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public List<Msisdn> getMSISDNWithFactory(SessionFactory fac) {
        List<Msisdn> data;
        try (Session session = fac.openSession()) {
            data = session.createQuery("from Msisdn", Msisdn.class).list();
            return data;
        }
    }

    public List<Msisdn> getMSISDN() {
        initCore();
        List<Msisdn> data;
        try (Session session = factory.openSession()) {
            data = session.createQuery("from Msisdn", Msisdn.class).list();
            return data;
        }
    }
}
