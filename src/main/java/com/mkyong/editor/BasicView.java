package com.mkyong.editor;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "dtBasicView")
public class BasicView implements Serializable {

    private List<Product> products;

    private ProductService service = new ProductService();

    public BasicView() {
        products = service.getProducts(10);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setService(ProductService service) {
        this.service = service;
    }
}
